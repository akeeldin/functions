# (c) 2021 economyx.AI
# 2022-01-27 (Thu)
# DCCLXXXVI
# 
# FRED API key requested using Google login for Akeel Din
# URL: https://fredaccount.stlouisfed.org/apikey
# API Key: 93ee9fd4d11c906b2ff941dbf3b7771c
# ---- Use This Function for Local Testing Purpose Only ----
# $ cd ~/repos/py3 
# $ source functions/bin/activate
# $ python ./fred_2pce.py
#
from fredapi import Fred
import pandas as pd

fred = Fred(api_key='93ee9fd4d11c906b2ff941dbf3b7771c')
#---Change This for Eact FRED Dataset---
labels = ('PCE', 'PCE_CHG')
#---------------------------------------
info = fred.get_series_info(labels[0])
print(info['notes'])
data = fred.get_series_latest_release(labels[0])
#print(type(data))
data = data.reset_index()
#print(data.tail(5))

df = pd.DataFrame(data)             #convert to Dataframe
df = df.rename(columns = {'index':'DATE', 0:labels[0]})
df[labels[1]] = df[labels[0]] - df[labels[0]].shift(1)
print(type(df))
print((df.tail()))




