# (c) 2021 economyx.AI
# 2022-01-26 (Tue)
# DCCLXXXVI
# 
# FRED API key requested using Google login for Akeel Din
# URL: https://fredaccount.stlouisfed.org/apikey
# API Key: 93ee9fd4d11c906b2ff941dbf3b7771c
# ---- Use This Function for Local Testing Purpose Only ----
# $ cd ~/repos/py3 
# $ source functions/bin/activate
# $ python ./fred_1gdi.py
#
from fredapi import Fred
import pandas as pd
from datetime import datetime

fred = Fred(api_key='93ee9fd4d11c906b2ff941dbf3b7771c')
labels = ('GDI', 'GDI_CHG')
info = fred.get_series_info(labels[0])
print(info['notes'])
date_string = info['last_updated']
split_string = date_string.split(" ", 1)
date_string = split_string[0]
n_date = datetime.now()
f_date = datetime.strptime(date_string, "%Y-%m-%d")
print (type(f_date))
delta = n_date - f_date
print (n_date)
print(type(f_date))
print ("type: ",type(f_date.strftime("%Y-%m-%d")))
print('Write update_date: ', datetime.now().strftime("%Y-%m-%d, %H:%M:%S"))

print (delta.days)

print(date_string)

data = fred.get_series_latest_release(labels[0])
#print(type(data))
data = data.reset_index()
#print(data.tail(5))

df = pd.DataFrame(data)             #convert to Dataframe
df = df.rename(columns = {'index':'DATE', 0:labels[0]})
df[labels[1]] = df[labels[0]] - df[labels[0]].shift(1)
print(type(df))
print((df.tail()))




