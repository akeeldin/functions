# An example requirements file, add your dependencies below
pandas
pandas_gbq
pandas.io
#gcsfs
google-cloud-secret-manager>=2.8.0
google-cloud-bigquery>=2.3.1
google-cloud-datastore>=2.4.0
fredapi
