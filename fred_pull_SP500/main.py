# (c) 2021 economyx.AI
# 2022-02-01 (Wed)
# DCCLXXXVI
# --- FRED_SP500 ---
# TThe Dow Jones Industrial Average provides a view of the US stock market and economy. Originally, the index was made up of 12 stocks, it now contains 30 component companies in various industries.
#
# FRED API key can be requested using Google login for Akeel Din URL: 
# https://fredaccount.stlouisfed.org/apikey
#
# docs for Google Secrets Manager:
# https://cloud.google.com/secret-manager/docs/creating-and-accessing-secrets
#
# make sure project is set to : panda-279900
# For testing
#    deploy gcf like this:
#    $ gcloud functions deploy fred_pull_pce --runtime python39 --trigger-http --allow-unauthenticated
# For Prod
#    deploy gcf like this:  
#    <see the note economyxAI|FRED API - GCF Functions>
#

#from flask import escape
import functions_framework
from fredapi import Fred
import pandas as pd
from pandas.io import gbq
from datetime import datetime

# Install Google Libraries
from google.cloud import bigquery
from google.cloud import secretmanager
from google.cloud import datastore

#---CHANGE THIS FOR EACH DATA SET---
labels = ('SP500', 'SP500_CHG')
#-----------------------------------
# (one more to change below)

# Setup the Secret manager Client
secrets = secretmanager.SecretManagerServiceClient()
# Get the secret FRED api code
PROJECT_ID = "198201164801"
FRED_API_KEY = secrets.access_secret_version(request={"name": "projects/"+PROJECT_ID+"/secrets/FRED_api_key/versions/1"}).payload.data.decode("utf-8")
project_id = 'panda-279900'

# Instantiates a Datastore client
datastore_client = datastore.Client()
bigquery_client = bigquery.Client(project=project_id)

"""
This function writes pandas dataframe into a bigquery table.
"""
def bq_load(tn, df):
    project_id = 'panda-279900'
    dataset_name = 'bbq3'
    table_name = tn

    # Define table name, in format dataset.table_name
    table_id = 'bbq3.FRED_'+labels[0]
  
    job_config = bigquery.LoadJobConfig(
        # Specify a (partial) schema. All columns are always written to the
        # table. The schema is used to assist in data type definitions.
        schema=[
            # Specify the type of columns whose type cannot be auto-detected. 
            bigquery.SchemaField("DATE", bigquery.enums.SqlTypeNames.DATETIME),
            # Indexes are written if included in the schema by name.
            bigquery.SchemaField(labels[1], bigquery.enums.SqlTypeNames.FLOAT64),
        ],
        # BigQuery appends loaded rows to an existing table by default,
        # but with WRITE_TRUNCATE write disposition it replaces the table with the loaded data.
        write_disposition="WRITE_TRUNCATE",
    )
    # Load data to BQ
    job = bigquery_client.load_table_from_dataframe(
        df, table_id, job_config=job_config
    )
    job.result()  # Wait for the job to complete.

"""
Pull date of last update to this dataset from FRED
"""
def get_fred_update_date(): 
    fred = Fred(api_key=FRED_API_KEY)
    info = fred.get_series_info(labels[0])
    date_string = info['last_updated']
    split_string = date_string.split(" ", 1)
    date_string = split_string[0]
    print("FRED Last Update to This Dataset: ", date_string)
    return date_string

"""
Check when our BigQuery table was last updated
"""
def get_table_update_date():

    # The kind for the new entity
    kind = "e_fred_data_updates"
    # The name/ID for the entity
    name = labels[0]
    key = datastore_client.key(kind, name)
    entity = datastore_client.get(key)
    if entity is None:                      #create if doesn't exist
        update_date = "1966-01-29, 05:30:00 AM"
        update_date_dt = datetime.strptime(update_date, "%Y-%m-%d, %I:%M:%S %p")
        status = datastore.Entity(key)
        status.update(
            {
                "update_date": update_date_dt,
                "update_status": "created",
            }
        ) 
        datastore_client.put(status)
        print("New data set didn't exist in BigQuery : ", name)
    else:
        update_date = entity['update_date']
        print("BigQuery Table was Last Updated: ", update_date)

    split_string = update_date.split(",", 1)  #YYYY-MM-DD
    update_date = split_string[0]
    return update_date

"""
Pull data from FRED site
"""
def get_api_data():
    #--Get the Data from FRED and upate to Bigquery
    fred = Fred(api_key=FRED_API_KEY)
    data = fred.get_series_latest_release(labels[0])
    data = data.reset_index()
    df = pd.DataFrame(data)                                     # convert Time Series data into Dataframe
    df = df.rename(columns = {'index':'DATE', 0:labels[0]})
    df[labels[1]] = df[labels[0]] - df[labels[0]].shift(1)      # Add a column for change in value from previous month.
    bq_load('FRED_'+labels[0], df)                              # load df to BigQuery

    #--Log this update in Datastore
    kind = "e_fred_data_updates"
    name = labels[0]
    key = datastore_client.key(kind, name)
    entity = datastore_client.get(key)
    entity["update_date"] = datetime.now().strftime("%Y-%m-%d, %H:%M:%S")
    entity["update_status"] = "success"
    datastore_client.put(entity) 
    print('Write update_date: ', datetime.now().strftime("%Y-%m-%d, %H:%M:%S"))

"""
Create ARIMA Plus model
See : https://cloud.google.com/bigquery-ml/docs/arima-single-time-series-forecasting-tutorial
"""
def create_model():
    query_job = bigquery_client.query(
        """
        CREATE OR REPLACE MODEL `panda-279900.bbq3.sp500_arima_model`
        OPTIONS
        (   model_type = 'ARIMA_PLUS',
            time_series_timestamp_col = 'date',
            time_series_data_col = 'sp500',
            auto_arima = TRUE,
            data_frequency = 'AUTO_FREQUENCY',
            decompose_time_series = TRUE
        ) AS
        SELECT date, sp500 FROM `panda-279900.bbq3.FRED_SP500`"""
    )

    results = query_job.result()  # Waits for job to complete.

    for row in results:
        print(row)

"""
Use model to forecast (with explain data) the timeseries
4 future time points (qtrs), and generate a prediction interval with a 90% confidence level.
See : https://cloud.google.com/bigquery-ml/docs/arima-single-time-series-forecasting-tutorial#step_six_use_your_model_to_forecast_the_time_series
"""
def forecast():
    query_job = bigquery_client.query(
        """
        DROP TABLE IF EXISTS `panda-279900.bbq3.sp500_forecast`;
        CREATE TABLE `panda-279900.bbq3.sp500_forecast` AS
        SELECT * FROM
        ML.EXPLAIN_FORECAST(MODEL bbq3.sp500_arima_model,
                STRUCT(365 AS horizon, 0.9 AS confidence_level))"""
    )

    results = query_job.result()  # Waits for job to complete.

    for row in results:
        print(row)


# functions_framework is an open source FaaS (Function as a service) framework for 
# writing portable Python functions -- brought to you by the Google Cloud Functions team.
# The Functions Framework lets you write lightweight functions that run in many different 
# environments, including Google Cloud Functions
@functions_framework.http
#---CHANGE THIS FOR EACH DATA SET---
def fred_pull_sp500(request):
#-----------------------------------
    """HTTP Cloud Function.
    Args:
        request (flask.Request): The request object.
        <https://flask.palletsprojects.com/en/1.1.x/api/#incoming-request-data>
    Returns:
        The response text, or any set of values that can be turned into a
        Response object using `make_response`
        <https://flask.palletsprojects.com/en/1.1.x/api/#flask.make_response>.
    """
    request_json = request.get_json(silent=True)
    request_args = request.args

    f_str = get_fred_update_date()
    t_str = get_table_update_date()
    f_date = datetime.strptime(f_str, "%Y-%m-%d")
    t_date = datetime.strptime(t_str, "%Y-%m-%d")
    n_date = datetime.now()
    delta = n_date - f_date
    if f_date > t_date:
        df = get_api_data()
        print("Successfully updated BigQuery Table")
        create_model()
        print("Successfully created new ARIMA model")
        forecast()  
        print("Successfully updated forecast table")
    else:
        print("No need to update BigQuery Table. Last update was (days): ", delta)
        
    return 'Success checking table FRED_'+labels[0]
