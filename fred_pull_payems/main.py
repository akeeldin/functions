# (c) 2021 economyx.AI
# 2022-01-05 (Wed)
# DCCLXXXVI
# --- FRED_PAYEMS ---
# All Employees: Total Nonfarm, commonly known as Total Nonfarm Payroll, is a measure of the number of U.S. workers in the economy 
# that excludes proprietors, private household employees, unpaid volunteers, farm employees, and the unincorporated self-employed. 
# This measure accounts for approximately 80 percent of the workers who contribute to Gross Domestic Product (GDP).  
# This measure provides useful insights into the current economic situation because it can represent the number of jobs added or 
# lost in an economy. Increases in employment might indicate that businesses are hiring which might also suggest that businesses are 
# growing. Additionally, those who are newly employed have increased their personal incomes, which means (all else constant) their 
# disposable incomes have also increased, thus fostering further economic expansion.  Generally, the U.S. labor force and levels 
# of employment and unemployment are subject to fluctuations due to seasonal changes in weather, major holidays, and the opening 
# and closing of schools. The Bureau of Labor Statistics (BLS) adjusts the data to offset the seasonal effects to show non-seasonal 
# changes: for example, women's participation in the labor force; or a general decline in the number of employees, a possible 
# indication of a downturn in the economy. To closely examine seasonal and non-seasonal changes, the BLS releases two monthly 
# statistical measures: the seasonally adjusted All Employees: Total Nonfarm (PAYEMS) and All Employees: Total Nonfarm (PAYNSA), 
# which is not seasonally adjusted.  The series comes from the 'Current Employment Statistics (Establishment Survey).'  
# The source code is: CES0000000001
#
# FRED API key can be requested using Google login for Akeel Din URL: 
# https://fredaccount.stlouisfed.org/apikey
#
# docs for Google Secrets Manager:
# https://cloud.google.com/secret-manager/docs/creating-and-accessing-secrets
#
# deploy gcg:
# gcloud functions deploy fred_pull_payems --runtime python39 --trigger-http --allow-unauthenticated

#from flask import escape
import functions_framework
from fredapi import Fred
import pandas as pd
from pandas.io import gbq
#import pandas_gbq
# Install Google Libraries
from google.cloud import bigquery
from google.cloud import secretmanager
#import gcsfs

# Setup the Secret manager Client
secrets = secretmanager.SecretManagerServiceClient()
# Get the secret FRED api code
PROJECT_ID = "198201164801"
FRED_API_KEY = secrets.access_secret_version(request={"name": "projects/"+PROJECT_ID+"/secrets/FRED_api_key/versions/1"}).payload.data.decode("utf-8")

"""
This function writes pandas dataframe into a bigquery table.
"""
def bq_load(tn, df):
  
    project_id = 'panda-279900'
    dataset_name = 'bbq3'
    table_name = tn
    # Load client
    client = bigquery.Client(project=project_id)

    # Define table name, in format dataset.table_name
    table_id = 'bbq3.FRED_PAYEMS'
  
    job_config = bigquery.LoadJobConfig(
        # Specify a (partial) schema. All columns are always written to the
        # table. The schema is used to assist in data type definitions.
        schema=[
            # Specify the type of columns whose type cannot be auto-detected. For
            # example the "title" column uses pandas dtype "object", so its
            # data type is ambiguous.
            bigquery.SchemaField("DATE", bigquery.enums.SqlTypeNames.DATETIME),
            # Indexes are written if included in the schema by name.
            bigquery.SchemaField("PAYEMS_CHG", bigquery.enums.SqlTypeNames.INT64),
        ],
        # Optionally, set the write disposition. BigQuery appends loaded rows
        # to an existing table by default, but with WRITE_TRUNCATE write
        # disposition it replaces the table with the loaded data.
        write_disposition="WRITE_TRUNCATE",
    )
    # Load data to BQ
    job = client.load_table_from_dataframe(
        df, table_id, job_config=job_config
    )   # Make an API request.
    job.result()  # Wait for the job to complete.

"""
Pull data from FRED site
"""
def get_api_data():
  
    fred = Fred(api_key=FRED_API_KEY)
    data = fred.get_series_latest_release('PAYEMS')
    data = data.reset_index()
    # convert Time Series data into Dataframe
    df = pd.DataFrame(data)
    df = df.rename(columns = {'index':'DATE', 0:'PAYEMS'})
    # Add a column for change in PAYEMS
    df['PAYEMS_CHG'] = df['PAYEMS'] - df['PAYEMS'].shift(1)
    # load df to BigQuery
    bq_load('FRED_PAYEMS', df)
 
# functions_framework is an open source FaaS (Function as a service) framework for 
# writing portable Python functions -- brought to you by the Google Cloud Functions team.
# The Functions Framework lets you write lightweight functions that run in many different 
# environments, including Google Cloud Functions
@functions_framework.http
def fred_pull_payems(request):
    """HTTP Cloud Function.
    Args:
        request (flask.Request): The request object.
        <https://flask.palletsprojects.com/en/1.1.x/api/#incoming-request-data>
    Returns:
        The response text, or any set of values that can be turned into a
        Response object using `make_response`
        <https://flask.palletsprojects.com/en/1.1.x/api/#flask.make_response>.
    """
    request_json = request.get_json(silent=True)
    request_args = request.args

    df = get_api_data()
    return 'Success loading to table FRED_PAYEMS'
