# (c) 2021 economyx.AI
# 2022-01-04 (Tue)
# DCCLXXXVI
# 
# FRED API key requested using Google login for Akeel Din
# URL: https://fredaccount.stlouisfed.org/apikey
# API Key: 93ee9fd4d11c906b2ff941dbf3b7771c
# ---- Use This Function for Local Testing Purpose Only ----
# $ python ./fred_payems.py
#
from fredapi import Fred
import pandas as pd

fred = Fred(api_key='93ee9fd4d11c906b2ff941dbf3b7771c')

info = fred.get_series_info('PAYEMS',)
print(info['notes'])
data = fred.get_series_latest_release('PAYEMS')
#print(type(data))
data = data.reset_index()
#print(data.tail(5))

df = pd.DataFrame(data)             #convert to Dataframe
df = df.rename(columns = {'index':'DATE', 0:'PAYEMS'})
df['PAYEMS_CHG'] = df['PAYEMS'] - df['PAYEMS'].shift(1)
print(type(df))
print((df.tail()))




